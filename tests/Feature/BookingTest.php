<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Carbon\Carbon;
use App\Booking;
use App\User;

class BookingTest extends TestCase
{            
    use DatabaseMigrations;

    /**
     * This function runs at the beggining of every test so we can seed the db
     */
    public function setUp()
    {
        parent::setUp();

        factory(Booking::class, 10)->create();
    }

    /**
     * @test A user can fetch all bookings
     */
    public function testFetchAllBookings()
    {
        // fetch all the bookings in order to compare
        $bookings = Booking::all();
        
        // send the request
        $response = $this->get('/bookings');
        
        // compare by counting how many booking we get from the back-end
        $response->assertJsonCount(count($bookings));
    }

    /**
     * @test A user can fetch an specific booking by id
     */
    public function testFetchSingleBooking()
    {
        // fetch a single booking in order to compare
        $booking = Booking::first();
        
        // send the request with the id of booking
        $response = $this->get("/bookings/$booking->id");
        
        // compare by searching in the response the reason for visitting
        $response->assertSee($booking->reason_for_visit);
    }

    /**
     * @test A user can create a new booking
     */
    public function testCreateBooking()
    {
        // get a user to relate to the new booking we want to create
        $user = factory(User::class)->create();
        
        // configure the booking information
        $booking = [
            'username' => $user->username,
            'reason_for_visit' => 'Got hit by a car',
            'start_date' => Carbon::parse('tomorrow'),
            'end_date' => Carbon::parse('tomorrow + 1 day'),
        ];
        
        // send the information to the back-end
        $this->post('/bookings', $booking);
        
        // search in the database for the booking to test if it was created
        $this->assertDatabaseHas('bookings', $booking);
    }

    /**
     * @test A user can edit an existing booking
     */
    public function testEditBooking()
    {
        // fetch a single booking in order to compare
        $booking = Booking::find(2);
        
        // Modifying the information
        $booking->reason_for_visit = 'Got hit by another car';
        
        // getting the data in the format expected by the back-end
        $fields = $booking->only([
            'username', 
            'reason_for_visit', 
            'start_date', 
            'end_date'
        ]);
        
        // sending the information of the booking
        $this->put("/bookings/$booking->id", $fields);
        
        // search in the database for the booking to test if it was edited
        $this->assertDatabaseHas('bookings', $fields);
    }

    /**
     * @test A user can delete a booking
     */
    public function testDeleteBooking()
    {
        // fetch a single booking in order to compare
        $booking = Booking::first();
        
        // send the request with the id of booking
        $this->delete("/bookings/$booking->id");
        
        // search in the database for the booking to test if it was deleted
        $this->assertDatabaseMissing('bookings', $booking->only('id'));
    }
}
