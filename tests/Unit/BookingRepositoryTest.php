<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Repositories\BookingRepository;
use Carbon\Carbon;
use App\Booking;
use App\User;

class BookingRepositoryTest extends TestCase
{            
    use DatabaseMigrations;

    protected $repository;

    /**
     * This function runs at the beggining of every test so we can seed the db
     */
    public function setUp()
    {
        parent::setUp();

        $this->repository = new BookingRepository(new Booking);
        factory(Booking::class, 10)->create();
    }

    /**
     * @test
     */
    public function it_can_create_a_booking()
    {
        // generate a fake booking without saving it into the database
        $mockup = factory(Booking::class)->make();
        
        // information to be saved via the repository
        $fields = [
            'username' => $mockup->username,
            'reason_for_visit' => $mockup->reason_for_visit, 
            'start_date' => $mockup->start_date, 
            'end_date' => $mockup->end_date,
        ];

        // saving the booking
        $booking = $this->repository->createBooking($fields);

        // comparing the mockup with reponse from repository
        $this->assertInstanceOf(Booking::class, $booking);
        $this->assertEquals($mockup->username, $booking->username);
        $this->assertEquals($mockup->reason_for_visit, $booking->reason_for_visit);
        $this->assertEquals($mockup->start_date, $booking->start_date);
        $this->assertEquals($mockup->end_date, $booking->end_date);
    }

    /**
     * @test
     */
    public function it_can_update_a_booking()
    {  
        // get an existing booking from the DB
        $booking = Booking::find(3);
        
        // updating without saving
        $booking->reason_for_visit = 'This an updated version of me :D!';

        // information to be saved via the repository
        $fields = [
            'username' => $booking->username,
            'reason_for_visit' => $booking->reason_for_visit, 
            'start_date' => $booking->start_date, 
            'end_date' => $booking->end_date,
        ];
        
        // the id needed to indicate which booking to update
        $id = $booking->id;
        
        // updating the booking
        $updated_booking = $this->repository->editBooking($fields, $id);

        // comparing the booking fetched manually with reponse from repository
        $this->assertInstanceOf(Booking::class, $updated_booking);
        $this->assertEquals($booking->username, $updated_booking->username);
        $this->assertEquals($booking->reason_for_visit, $updated_booking->reason_for_visit);
        $this->assertEquals($booking->start_date, $updated_booking->start_date);
        $this->assertEquals($booking->end_date, $updated_booking->end_date);
    }

    /**
     * @test
     */
    public function it_can_delete_a_booking()
    {
        // get an existing booking from the DB
        $booking = Booking::find(7);
        
        // the id needed to indicate which booking to delete
        $id = $booking->id;
        
        // deleting the booking
        $result = $this->repository->deleteBooking($id);

        // checking for response to be true
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function it_can_fetch_a_booking_by_id()
    {
        // get an existing booking from the DB
        $booking = Booking::find(5);
        
        // the id needed to indicate which booking to retrieve
        $id = $booking->id;

        // fetching the booking
        $fetched_booking = $this->repository->getBooking($booking->id);

        // comparing the booking fetched manually with the one from repository
        $this->assertInstanceOf(Booking::class, $fetched_booking);
        $this->assertEquals($booking->username, $fetched_booking->username);
        $this->assertEquals($booking->reason_for_visit, $fetched_booking->reason_for_visit);
        $this->assertEquals($booking->start_date, $fetched_booking->start_date);
        $this->assertEquals($booking->end_date, $fetched_booking->end_date);
    }

    /**
     * @test
     */
    public function it_can_fetch_all_bookings()
    {
        // getting all bookings from DB
        $bookings = Booking::all();

        // doing the same from repository
        $fetched_bookings = $this->repository->getAllBookings();

        // comparing at least to have the same amount of records
        $this->assertEquals(count($bookings), count($fetched_bookings));
    }
}
