<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'reason_for_visit',
        'start_date',
        'end_date',
    ];

    /**
     * Creates an eloquent relationship between bookings and users.
     * 
     * @return BelongsTo instance to retrieve the user owner of the booking.
     */
    public function user()
    {
        /* Normally this creates the relationship with the id field, but we can
         * arguments to the function to customize this behaviour.
         */
        return $this->belongsTo(User::class, 'username', 'username');
    }
}
