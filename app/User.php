<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Creates an eloquent relationship between users and bookings.
     * 
     * @return HasMany instance to retrieve the bookings belonging to the user.
     */
    public function bookings()
    {
        /* Normally this creates the relationship with the id field, but we can
         * arguments to the function to customize this behaviour.
         */
        return $this->hasMany(Booking::class, 'username', 'username');
    }
}
