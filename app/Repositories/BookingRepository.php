<?php

namespace App\Repositories;

use App\Booking;

class BookingRepository
{
    
    protected $booking;

    function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Creates a new booking with the data provided
     * @param  array  $fields
     * @return Booking created
     */
    public function createBooking(array $fields)
    {
        return $this->booking->create($fields);
    }

    /**
     * Updates the booking with the data provided based on the id
     * @param  array  $fields
     * @param  int    $id
     * @return Booking updated
     */
    public function editBooking(array $fields, int $id)
    {
        // update returns true, to return an updated representation of the resource
        // we call the method to get a single booking
        $this->booking->find($id)->update($fields);
        return $this->getBooking($id);
    }

    /**
     * Deletes a booking with the id provided
     * @param  int    $id
     * @return bool true on success, false on failure
     */
    public function deleteBooking(int $id)
    {
        return $this->booking->find($id)->delete();
    }

    /**
     * Retrieves a single booking with the id provided
     * @param  int    $id 
     * @return Booking or null if no booking with the id provided was found
     */
    public function getBooking(int $id)
    {
        return $this->booking->find($id);
    }

    /**
     * Retrieves all instances of bookings
     * @return Collection of Booking
     */
    public function getAllBookings()
    {
        return $this->booking->all();
    }    
}