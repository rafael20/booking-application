<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\BookingRepository;
use App\Booking;
use Validator;

class BookingController extends Controller
{

    protected $repository;

    /**
     * [__construct description]
     * @param BookingRepository $repository [description]
     */
    function __construct(BookingRepository $repository)
    {
        $this->repository = $repository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->repository->getAllBookings();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request);

        // form validation
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        // extracting data from request to build the booking object
        $fields = $request->only([
            'username', 
            'reason_for_visit', 
            'start_date', 
            'end_date'
        ]);

        // creating the booking through repository
        $booking = $this->repository->createBooking($fields);

        return $booking;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        /** 
         * Though we could use the repository like so:
         *  
         * $booking = $this->repository->getBooking($booking->id);
         *
         * It is completely unnecesary since laravel automatically resolve 
         * the model from the id included in the request. This is called,
         * route model binding, more on this:
         * 
         * https://laravel.com/docs/5.6/routing#route-model-binding
         */
        
        return $booking;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        $validator = $this->validator($request);

        // form validation
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        // extracting data from request to update the booking object
        $fields = $request->only([
            'username', 
            'reason_for_visit', 
            'start_date', 
            'end_date'
        ]);

        // getting the id of the booking to update
        $id = $booking->id;

        // updating booking through repository
        $updated_booking = $this->repository->editBooking($fields, $id);

        // returning updated representation of the resource
        return $updated_booking;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        $id = $booking->id;
        return $this->repository->deleteBooking($id);
    }

    /**
     * Creates an instance of the Validator class to ensure that the same rules
     * are applied to bookings when creating or updating a record.
     *
     * @param Request $request
     * @return Validator instance
     */
    private function validator(Request $request)
    {
        return Validator::make($request->all(), [
            'username' => 'required|max:32|exists:users,username',
            'reason_for_visit' => 'required|max:250',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ]);
    }
}
