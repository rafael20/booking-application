<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
use App\Booking;
use App\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Booking::class, function (Faker $faker) {    
    return [
        'username' => function() {
            return factory(App\User::class)->create()->username;
        },
        'reason_for_visit' => $faker->unique()->sentence,
        'start_date' => Carbon::parse('tomorrow')->toDateTimeString(),
        'end_date' => Carbon::parse('tomorrow + 1 day')->toDateTimeString(),
    ];
});
