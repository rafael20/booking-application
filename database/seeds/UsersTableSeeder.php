
<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the user's table
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'admin',
            'name' => 'Mr. Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
        ]);

        User::create([
            'username' => 'john_doe',
            'name' => 'John Doe',
            'email' => 'john_doe@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
