<?php

use Illuminate\Database\Seeder;
use App\Booking;
use App\User;
use Carbon\Carbon;

class BookingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::orderByDesc('id')->first();

        Booking::create([
            'username' => $user->username,
            'reason_for_visit' => 'Got hit by a car',
            'start_date' => Carbon::parse('tomorrow'),
            'end_date' => Carbon::parse('tomorrow + 1 day'),
        ]);
    }
}
