<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/**
 * This provides quick access to the following routes:
 *  GET      /bookings            -> BookingController@index
 *  GET      /bookings/{id}       -> BookingController@show
 *  POST     /bookings           -> BookingController@store
 *  PUT      /bookings/{id}       -> BookingController@update
 *  DELETE   /bookings/{id}    -> BookingController@destroy
 */
Route::resource('/bookings', 'BookingController')->except(['create', 'edit']);