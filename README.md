# Booking Application

A simple booking application designed for GroupHEALTH in order assess PHP skills

## Setup Environment

* Run `cp .env.example .env` and update database configuration in .env 
* Run `php artisan key:generate`
* Run `php artisan migrate --seed`
* Run `npm install && npm run dev`

## Enable Laravel to write on cache and logs

* Run `sudo chown -R www-data:www-data storage OR sudo chmod -R 777 storage`
* Run `sudo chown www-data:www-data bootstrap/cache`

## Files to Assess

#### Business Logic of the application:
* [app/User.php](app/User.php)
* [app/Booking.php](app/Booking.php)
* [app/Repositories/BookingRepository.php](app/Repositories/BookingRepository.php)
* [app/Http/Controllers/BookingController.php](app/Http/Controllers/BookingController.php)
* [routes/web.php](routes/web.php)

#### Unit Tests and Database Seeding:
* [database/migrations/](database/migrations/)
* [database/seeds/](database/seeds/)
* [database/factories/BookingFactory.php](database/factories/BookingFactory.php)
* [tests/Feature/BookingTest.php](tests/Feature/BookingTest.php)
* [tests/Unit/BookingRepositoryTest.php](tests/Unit/BookingRepositoryTest.php)
