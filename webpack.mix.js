let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
        'resources/assets/js/jquery.3.2.1.min.js',
        'resources/assets/js/bootstrap.min.js',
        'resources/assets/js/chartist.min.js',
        'resources/assets/js/bootstrap-notify.js',
        'resources/assets/js/light-bootstrap-dashboard.js',
        'resources/assets/js/demo.js',
    ], 'public/js/app.js')
    .styles([
        'resources/assets/css/bootstrap.min.css',
        'resources/assets/css/animate.min.css',
        'resources/assets/css/pe-icon-7-stroke.css'
    ], 'public/css/vendor.css')
    .copyDirectory('resources/assets/img', 'public/img')
    .copyDirectory('resources/assets/fonts', 'public/fonts')
    .sass('resources/assets/sass/app.scss', 'public/css');
