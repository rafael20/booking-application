<div class="sidebar" data-color="purple" data-image="img/sidebar-5.jpg">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                Creative Tim
            </a>
        </div>
        <ul class="nav">
            <li class="active">
                <a href="table.html">
                    <i class="pe-7s-note2"></i>
                    <p>Bookings</p>
                </a>
            </li>
            <li>
                <a href="user.html">
                    <i class="pe-7s-user"></i>
                    <p>User Profile</p>
                </a>
            </li>
        </ul>
    </div>
</div>