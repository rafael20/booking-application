<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />    

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>    

    <!-- Styles -->
    <link href="{{ asset('css/vendor.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Fonts and Icons-->    
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <style>
        .full-width {
            width: 100%;
        }
    </style>
</head>
<body>
    <div class="wrapper">

        @includeWhen(Auth::check(), 'layouts.sidebar')
        
        <div class="main-panel {{ !Auth::check() ? 'full-width' : '' }}">
            
            @include('layouts.nav')
            
            @yield('content')
            
            @include('layouts.footer')

        </div>
        
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>

        @yield('script')

    </div>
</body>
</html>
